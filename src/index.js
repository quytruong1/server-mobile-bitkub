const express = require('express');
const cors = require('cors');
const userRoutes = require("./routes/user");
const bodyParser = require('body-parser');


const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

app.use("/api/users", userRoutes);


const port = process.env.PORT || 8000;
app.listen(port, console.log(`Listening on port ${port}...`));